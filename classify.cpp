#include <iostream>
#include <vector>
#include "image_ppm.h"

using namespace std;

bool closestCol(OCTET* im, OCTET* blueIm, OCTET* redIm,int pix){

    if((abs(im[pix]-blueIm[pix])+abs(im[pix+1]-blueIm[pix+1])+abs(im[pix+2]-blueIm[pix+2])
            <
            abs(im[pix]-redIm[pix])+abs(im[pix+1]-redIm[pix+1])+abs(im[pix+2]-redIm[pix+2]))){
                return 0;
            }
            else{return 1;}

}

int main(int argc, char* argv[]){

    
    // open 2 base blue/red images. 
    char blue[250]; sprintf(blue,"projectedImages/blue.ppm");
    char red[250]; sprintf(red,"projectedImages/red.ppm");
    int nH, nW;

    OCTET* blueIm; OCTET* redIm;
    lire_nb_lignes_colonnes_image_ppm(blue,&nH,&nW);
    allocation_tableau(blueIm, OCTET, nH*nW*3);
    allocation_tableau(redIm, OCTET, nH*nW*3);

    lire_image_ppm(blue,blueIm,nH*nW);
    lire_image_ppm(red,redIm,nH*nW);

    int iter=10;

    //contient iter images
    vector<OCTET*> listeIms;

    //contient la classification obtenue pour chaque pixel et chaque itération
    // 0 blue ; 1 red
    vector<vector<bool>> classif;
    char nom[250];
    for (int i=0; i<iter; i++){
        classif.push_back(vector<bool>());
        sprintf(nom,"projectedImages/%d.ppm",i+1);
        OCTET* im; listeIms.push_back(im);
        allocation_tableau(listeIms[i],OCTET, nH*nW*3);
        lire_image_ppm(nom,listeIms[i],nH*nW);
        for (int pix=0; pix<nH*nW;pix+=3){
            if((abs(listeIms[i][pix]-blueIm[pix])+abs(listeIms[i][pix+1]-blueIm[pix+1])+abs(listeIms[i][pix+2]-blueIm[pix+2])
            <
            abs(listeIms[i][pix]-redIm[pix])+abs(listeIms[i][pix+1]-redIm[pix+1])+abs(listeIms[i][pix+2]-redIm[pix+2]))){
                classif[i].push_back(0);
            }
            else{classif[i].push_back(1);}
        }
    }

for (int l=0;l<nW;l++){
    for (int i=0; i<iter;i++){
        cout<<classif[i][l];
    }
    cout<<endl;
}

}