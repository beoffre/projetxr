#include <iostream>
#include <vector>
#include "image_ppm.h"

using namespace std;

int main(int argc, char* argv[]){

    
    
    
    int nH=1080, nW=1920;

    OCTET* blueIm; OCTET* redIm;

    int nTaille=nH*nW, nTaille3=3*nTaille;
    
    allocation_tableau(blueIm, OCTET, nH*nW*3);
    allocation_tableau(redIm, OCTET, nH*nW*3);

    for (int i=0; i<nH*nW*3;i+=3){
        blueIm[i]=0;
        blueIm[i+1]=0;
        blueIm[i+2]=255;

        redIm[i]=255;
        redIm[i+1]=0;
        redIm[i+2]=0;
    }

    ecrire_image_ppm("blue.ppm",blueIm,nH,nW);
    ecrire_image_ppm("red.ppm",redIm,nH,nW);

    

    int iter=10;
    int begin = 2;
    OCTET* im;
    for (int i=0; i<iter;i++){
        
        allocation_tableau(im,OCTET, nH*nW*3);

        bool lastColor=0;
        for (int pix=0;pix<nTaille;pix++){
            //if (((pix%nW)/(nW/(int)(pow(2,i+1))))%2){
                if (lastColor){
                    im[3*pix]=255;
                    im[3*pix+1]=0;
                    im[3*pix+2]=0;
                }
                else{
                    im[3*pix]=0;
                    im[3*pix+1]=0;
                    im[3*pix+2]=255;
                }
                if (((pix+1)%nW)%(begin)==0){
                    lastColor=!lastColor;
                    pix++;
                    im[3*pix]=0;
                    im[3*pix+1]=255;
                    im[3*pix+2]=0;
                    pix++;
                    im[3*pix]=0;
                    im[3*pix+1]=255;
                    im[3*pix+2]=0;
                }
            // if (<begin/2){
                
            // }
            // else{
            //     im[3*pix]=255;
            //     im[3*pix+1]=0;
            //     im[3*pix+2]=0;
            // }
            // if (((pix+1)%nW)%(begin/2)==0){
            //     pix++;
            //     im[3*pix]=0;
            //     im[3*pix+1]=255;
            //     im[3*pix+2]=0;
            //     pix++;
            //     im[3*pix]=0;
            //     im[3*pix+1]=255;
            //     im[3*pix+2]=0;
            // }
        }
        begin*=2;
        char nom[50]; sprintf(nom,"images/im%d.ppm",i);
        ecrire_image_ppm(nom,im,nH,nW);
        //free(im);
    }

    cout<<(int)im[0]<<" "<<(int)im[1]<<" "<<(int)im[2]<<endl;
    cout<<(int)im[3]<<" "<<(int)im[4]<<" "<<(int)im[5]<<endl;
    cout<<(int)im[6]<<" "<<(int)im[7]<<" "<<(int)im[8]<<endl;
    cout<<(int)im[9]<<" "<<(int)im[10]<<" "<<(int)im[11]<<endl;
    
}