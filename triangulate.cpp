#include "vec.h"
#include <iostream>
#include <vector>

#include "image_ppm.h"

using namespace std;

int main(){


    char classifName[250]; sprintf(classifName,"nomPixels.pgm");
    OCTET* classif;

    int nWCam = 254;
    int nHCam = 314;

    vector<vec3> points;

    allocation_tableau(classif, OCTET, nWCam*nHCam);

    lire_image_pgm(classifName,classif,nWCam*nHCam);


    vec3 EpCam = vec3(-0.47,0.0,0.0);
    vec3 EpProj = vec3(0.0,0.0,0.0);

    vec3 ULCam = vec3(-0.57,0.1,0.1);
    vec3 ULProj = vec3(-0.1,0.1,0.1);

    vec3 BRCam = vec3(-0.37,-0.1,0.1);
    vec3 BRProj = vec3(0.1,-0.1,0.1);

    

    int nWProj = 64;
    int nHProj = nHCam;

    vec3 u = vec3(EpProj - EpCam);

    for (int i=0;i<nHCam;i++){

        vec3 posCamAvant = ULCam -1.0/(float)nWCam*vec3(0.2,0.0,0.0) + (float)i/(float)nHCam*vec3(0.0,-0.2,0.0);

        vec3 v = vec3(posCamAvant - EpCam);
        
        for (int j=0;j<nWCam;j++){

            int val = classif[i*nWCam+j];

            if (val!=255){

                vec3 posPointCam = ULCam + (float)j/(float)nWCam*vec3(0.2,0.0,0.0) + (float)i/(float)nHCam*vec3(0.0,-0.2,0.0);

                

                

                vec3 posPointProj = ULProj + (float)i/(float)nHProj*vec3(0.0,-0.2,0.0) + (float)val/(float)nWProj*vec3(0.2,0.0,0.0);

                float xp1=dot(posPointCam - EpCam,u), yp1= dot(posPointCam - EpCam,v) , xe1=0.0, ye1=0.0, xe2=1.0, ye2=0.0, xp2=dot(EpProj-EpCam,posPointProj-EpCam) ,yp2=dot(posPointCam-EpCam,posPointProj-EpCam);
                //xp2=dot(EpProj-EpCam,posPointProj-EpCam)/dot(EpProj-EpCam,EpProj-EpCam) ,yp2=dot(posPointCam-EpCam,posPointProj-EpCam)/dot(posPointCam-EpCam,posPointCam-EpCam);

                float a = (yp1-ye1)/(xp1-xe1);
                float ap = (yp2-ye2)/(xp2-xe2);

                //cout<<a<<", "<<ap<<endl;

                float b = yp1-xp1*a;
                float bp = yp2-xp2*ap;
                //cout<<b<<", "<<bp<<endl;

                float x = (-b+bp)/(a-ap);
                float y = b+ a*x;

                vec3 res = vec3(x*u+y*v);
                cout<<res.x<<"  "<<res.y<<"  "<<res.z<<"\n";
            }
        }
    }
        
}