#include <iostream>
#include <vector>
#include "image_ppm.h"

using namespace std;


int closestCol(OCTET* im, OCTET* blueIm, OCTET* redIm,int pix){
    if (abs((((im[pix]-blueIm[pix])+(im[pix+1]-blueIm[pix+1])+(im[pix+2]-blueIm[pix+2])))
            -
            (((im[pix]-redIm[pix])+(im[pix+1]-redIm[pix+1])+(im[pix+2]-redIm[pix+2]))))<30){return 127;}

    if((abs(im[pix]-blueIm[pix])+abs(im[pix+1]-blueIm[pix+1])+abs(im[pix+2]-blueIm[pix+2])
            <
            abs(im[pix]-redIm[pix])+abs(im[pix+1]-redIm[pix+1])+abs(im[pix+2]-redIm[pix+2]))){
                return 0;
            }
            else{return 255;}

}

int main(){
    char blue[250]; sprintf(blue,"cropped_ppm/blue.ppm");
    char red[250]; sprintf(red,"cropped_ppm/red.ppm");
    
    int nH, nW;

    OCTET* blueIm; OCTET* redIm; OCTET* imN;
    lire_nb_lignes_colonnes_image_ppm(blue,&nW,&nH);
    allocation_tableau(blueIm, OCTET, nH*nW*3);
    allocation_tableau(redIm, OCTET, nH*nW*3);
    allocation_tableau(imN, OCTET, nH*nW*3);

    lire_image_ppm(blue,blueIm,nH*nW);
    lire_image_ppm(red,redIm,nH*nW);

    
    int iter=8;
    for (int i=0; i<iter; i++){
        char nameN[250]; sprintf(nameN,"cropped_ppm/color%d.ppm",i+1);
        lire_image_ppm(nameN,imN,nH*nW);
    

        OCTET* classif; allocation_tableau(classif,OCTET,nH*nW);
        for (int pix=0; pix<nH*nW*3;pix+=3){
            classif[pix/3]=closestCol(imN,blueIm,redIm,pix);
        }
        char namePrint[50]; sprintf(namePrint, "cropped_ppm/classif%d.pgm",i+1);
        ecrire_image_pgm(namePrint,classif,nW,nH);
    }


}