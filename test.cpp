#include <iostream>
#include "image_ppm.h"
using namespace std;

int main(){

    int nH=314, nW=254;

    OCTET* res; allocation_tableau(res, OCTET, nH*nW);
    for (int i=0;i<nH*nW;i++){res[i]=0;}

    for (int i=0; i<5; i++){
        OCTET* imN;
        allocation_tableau(imN, OCTET, nH*nW);
        char nameN[250]; sprintf(nameN,"cropped_ppm/classif%d.pgm",i+1);
        lire_image_pgm(nameN,imN,nH*nW);
    

        for (int pix=0;pix<nH*nW;pix++){
            if (imN[pix]==127){
                res[pix]=255;
            }
            else{
                if (res[pix]!=255){
                    res[pix]=res[pix]*2;
                    if (imN[pix]==0){
                        res[pix]+=1;
                    }
                }
                
            }
        }
    }
    ecrire_image_pgm("nomPixels.pgm",res,nH,nW);
}